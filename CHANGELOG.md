## [9.0.1](https://gitlab.com/Orange-OpenSource/lfn/infra/rke2_automatic_installation_collection/compare/1.5.2...9.0.1) (2023-02-05)


### Bug Fixes

* **deps:** update galaxy packages ([2e8d4d56](https://gitlab.com/Orange-OpenSource/lfn/infra/rke2_automatic_installation_collection/commit/2e8d4d56ca7ce64ae08bcd4cbe9407f75e27b2fc))

  | datasource        | package                                           | from  | to    |
  | ----------------- | ------------------------------------------------- | ----- | ----- |
  | gitlab-releases   | Orange-OpenSource/lfn/infra/kubernetes_collection | 2.1.7 | 9.0.4 |
  | gitlab-releases   | Orange-OpenSource/lfn/infra/infra_collection      | 9.0.2 | 9.0.3 |
  | galaxy-collection | community.general                                 | 6.2.0 | 6.3.0 |
  | galaxy-collection | kubernetes.core                                   | 2.3.2 | 2.4.0 |
