FROM registry.gitlab.com/orange-opensource/lfn/ci_cd/docker-ansible-core:2.14

ARG CI_COMMIT_SHORT_SHA
ARG BUILD_DATE

LABEL org.opencontainers.image.source="https://gitlab.com/Orange-OpenSource/lfn/infra/rke2_automatic_installation_collection" \
    org.opencontainers.image.ref.name="RKE2 Installation collection by Orange" \
    org.opencontainers.image.authors="Sylvain Desbureaux <sylvain.desbureaux@orange.com> \
    Nicolas Edel <nicolas.edel@orange.com>" \
    org.opencontainers.image.licenses=" Apache-2.0" \
    org.opencontainers.image.revision=$CI_COMMIT_SHORT_SHA

COPY ansible.cfg /etc/ansible/
ENV APP /opt/rke2_automatic_installation/
ENV COLLECTION_PATH ~/.ansible/collections

WORKDIR $APP

COPY . $APP/

RUN mkdir -p "$COLLECTION_PATH" && \
    ansible-galaxy collection install "git+file://$PWD/.git" \
      -p "$COLLECTION_PATH" && \
    rm -rf "$APP/.git" && \
    rm /root/.ansible/collections/ansible_collections/community/general/plugins/modules/java_keystore.py
