# Configure Playbook

## Purpose

Configure nodes for an rke2 installation.

In addition to the inventory, opnfv PDF and IDF compatible files must be
provided to describe the nodes.

## Inventory

The inventory must provide a `k8s-cluster` group to which all cluster nodes must
belong to.

Search for the `idf.yml`, `pdf.yml` and optionnal `project_infos.yml` vars files
happens at the same level as inventory directory.

```shell
.
├── inventory
│   └── inventory
└── vars
    └── idf.yml
    └── pdf.yml
    └── project_infos.yml
```

## Parameters

This playbook doesn't use specific parameters but those required by the invoked
roles.

Check parameters of the [orange.rke2.configure](../roles/configure.md) and
[orange.rke2.external_requirements](../roles/external_requirements.md) roles for
more informations
